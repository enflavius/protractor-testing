const EC = protractor.ExpectedConditions;
const Page = require('./page');

class AngularPage extends Page {
  constructor() {
    super('http://angular.io');
    
    this.navLinks = {
      "DOCS": {
        element: element(by.css('.nav-link[href="docs"]')),
        url: "/docs"
      },
      "RESOURCES": {
        element: element(by.css('.nav-link[href="resources"]')),
        url: "/resources"
      }
    }
    
    this.searchElement = element(by.css('input[aria-label="search"]'));
    this.searchResultsElement = element(by.css('.search-results'));
    this.searchResultsColumn1TitleElement = element(by.css('.search-results .search-area>h3:nth-of-type(1)'))
    this.searchResultsColumn2TitleElement = element(by.css('.search-results .search-area>h3:nth-of-type(2)'))
    this.searchResultsColumn3TitleElement = element(by.css('.search-results .search-area>h3:nth-of-type(3)'))
    this.buttonCTA = element(by.css('.button.hero-cta'));
    this.featuresNavLink = element(by.css('.nav-link[href="features"]'));
  }
  
  clickButtonCTA() {
    browser.wait(EC.elementToBeClickable(this.buttonCTA));
    return this.buttonCTA.click();
  }
  
  clickFeaturesLink() {
    return this.featuresNavLink.click();
  }
  
  clickNavLink(navLinkId) {
    return this.navLinks[navLinkId].element.click();
  }
  
  getUrlFor(navLinkId) {
    return this.navLinks[navLinkId].url;
  }
  
  checkLinkUrl(link) {
    expect(browser.getCurrentUrl()).toContain(this.getUrlFor(link));
  }
  
  searchText(text) {
    this.searchElement.sendKeys(text);
  }
  
  searchResultsIsDisplayed() {
    browser.wait(EC.presenceOf(this.searchResultsElement));
    return this.searchResultsElement.isDisplayed();
  }
  
  searchResultsColumn1Title() {
    browser.wait(EC.presenceOf(this.searchResultsColumn1TitleElement));
    return this.searchResultsColumn1TitleElement.getText();
  }
  
  searchResultsColumn2Title() {
    browser.wait(EC.presenceOf(this.searchResultsColumn2TitleElement));
    return this.searchResultsColumn2TitleElement.getText();
  }
  
  searchResultsColumn3Title() {
    browser.wait(EC.presenceOf(this.searchResultsColumn3TitleElement));
    return this.searchResultsColumn3TitleElement.getText();
  }
}

module.exports = AngularPage;