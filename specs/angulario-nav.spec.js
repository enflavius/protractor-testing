describe('Angular.io homepage navigation', function() {

  const AngularPage = require('./angular-page');
  const angularPage = new AngularPage();


  beforeEach(function(){
    // Load the Angular.io homepage.
    angularPage.openPage();
  });

  it('Given I am on angular.io homepage' +
    ' When I click on "Getting started"' +
    ' Then I should be redirected to quick start page', function() {

    // Select the "Getting started" button and click
    angularPage.clickButtonCTA();

    expect(browser.getCurrentUrl()).toContain('quickstart');
  });

  it('Given I am on angular.io homepage' +
    ' When I click on "Features"' +
    ' Then I should be redirected to features page', function() {

    // Select the "Getting started" button and click
    angularPage.clickFeaturesLink();

    expect(browser.getCurrentUrl()).toContain('features');
  });
  
  for (let link in angularPage.navLinks) {
    
    it('Given I am on angular.io homepage' +
      ' When I click on "Docs"' +
      ' Then I should be redirected to docs page', function() {
    
      // Select the "Getting started" button and click
      angularPage.clickNavLink(link);
    
      angularPage.checkLinkUrl(link);
    });
  }
  
  it('Given I am on angular.io homepage' +
    ' When I click on "Docs"' +
    ' Then I should be redirected to docs page', function() {
    
    // Select the "Getting started" button and click
    angularPage.searchText("model")
    
    expect(angularPage.searchResultsIsDisplayed()).toBe(true);
    expect(angularPage.searchResultsColumn1Title()).toEqual("API (23)");
    expect(angularPage.searchResultsColumn2Title()).toEqual("GUIDE (28)")
    expect(angularPage.searchResultsColumn3Title()).toEqual("TUTORIAL (2)")
  });
});
