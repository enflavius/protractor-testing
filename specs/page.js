class Page {
  
  constructor(pageUrl) {
    this.pageUrl = pageUrl;
  }
  
  openPage() {
    return browser.get(this.pageUrl);
  }
}

module.exports = Page;