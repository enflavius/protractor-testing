// An example configuration file
exports.config = {
  // The address of a running selenium server.
  seleniumAddress: 'http://localhost:4444/wd/hub',

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    browserName: 'chrome',
    shardTestFiles: true,
    maxInstances: 2,
    count: 1
  },

  // Spec patterns are relative to the configuration file location passed
  // to protractor (in this example conf.js).
  // They may include glob patterns.
  //specs: ['specs/*.spec.js'],
  /*suites: {
    angular: ['specs/angular*spec.js']
  },*/
  
  // Cucumber
  specs: [
    'features/*.feature'
  ],
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    require: [
      'hooks.js',
      'features/step_definitions/*step_definitions.js'
      ],
    tags: ['@ui', '~@skip'],
    format: ['progress', 'pretty'],
    profile: false,
    'no-source': true
  },

  // Options to be passed to Jasmine-node.
  onPrepare: function() {
    /*const reporters = require('jasmine-reporters');
    const junitReporter = new reporters.JUnitXmlReporter({
      savePath: __dirname + '/reports',
      consolidateAll: true
    });
    jasmine.getEnv().addReporter(junitReporter);

    const SpecReporter = require('jasmine-spec-reporter').SpecReporter;
    jasmine.getEnv().addReporter(new SpecReporter({
      spec: {
        displayStacktrace: true
      }
    }));*/
  },
  /**
   * Protractor log level
   *
   * default: INFO
   */
  logLevel: 'INFO', //'ERROR'|'WARN'|'INFO'|'DEBUG'

  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true, // Use colors in the command line report.
  },
};
