# protractor-testing

## Configure and run suites

Add suites to protractor config as follows:
suites: {
    suiteA: [file1,file2],
    suiteB: [file1, file2]
}

Install packages (once)
> npm install

Update selenium server & drivers (once)
> npm run webdriver-update

Start selenium server
> npm run webdriver-start

Run tests
> npm test

Cucumber

https://github.com/protractor-cucumber-framework/protractor-cucumber-framework/blob/master/README.md
https://github.com/cucumber/cucumber-js
https://www.chaijs.com/
https://github.com/domenic/chai-as-promised
