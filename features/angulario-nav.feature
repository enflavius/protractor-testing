@ui
Feature: Running Cucumber with Protractor
  As a user of Protractor
  I should be able to use Cucumber
  In order to run my E2E tests

  @navigation
  Scenario Outline: Angular.io homepage navigation search
    Given I am on angular.io homepage
    When I click on "<link>"
    Then I should be redirected to "<link>" page

    Examples:
      | link      |
      | DOCS      |
      | RESOURCES |