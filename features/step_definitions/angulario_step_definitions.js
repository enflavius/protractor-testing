const { Given, When, Then, setDefaultTimeout } = require('cucumber');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;

const AngularPage = require('../../specs/angular-page');
const angularPage = new AngularPage();

setDefaultTimeout(60000);

Given(/^I am on angular.io homepage$/, () => {
  return angularPage.openPage().then((response)=> {
    return return browser.get()
  });
});

When(/^I click on "([^"]*)"$/, (link) => {
  return angularPage.clickNavLink(link);
});

Then(/^I should be redirected to "([^"]*)" page$/, (link) => {
  return expect(browser.getCurrentUrl()).to.eventually.contain(angularPage.getUrlFor(link));
});